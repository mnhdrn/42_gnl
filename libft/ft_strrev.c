/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 22:30:18 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/04 22:31:13 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_strrev(char *s)
{
	char *s_end;
	char tmp;

	s_end = s + ft_strlen(s) - 1;
	while (s < s_end)
	{
		tmp = *s;
		*s++ = *s_end;
		*s_end-- = tmp;
	}
}
