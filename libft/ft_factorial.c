/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_factorial.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 12:40:54 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/15 12:44:27 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_factorial(size_t nb)
{
	int			result;

	if (nb == 0 || nb == 1)
		return (1);
	else if (nb >= 2 && nb < 13)
		result = (nb * ft_factorial(nb - 1));
	else
		return (0);
	return (result);
}
