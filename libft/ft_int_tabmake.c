/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_int_tabmake.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 18:46:59 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/10 19:15:11 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				*ft_int_tabmake(size_t size)
{
	int			*tab;
	size_t		i;

	if (!(tab = (int *)malloc(sizeof(int) * size)))
		return (NULL);
	i = 0;
	while (i < size)
		tab[i++] = 0;
	return (tab);
}
